<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        dd($request->all());

        DB::table('cast')->insert(
            [
                'nama'=>$request['nama'],
                'umur'=>$request['umur'],
                'bio'=>$request['bio']
            ]
        );

        return redirect('/');
    }
}
