<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/', function(){
    return view('layout.master');
})*/

Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@register');

Route::post('/kirim', 'AuthController@welcome');

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

