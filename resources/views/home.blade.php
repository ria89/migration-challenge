<!DOCTYPE html>
<html>
    <head>
        <title>Index</title>
    </head>

    <body>
        <h1>Media Online</h1>
        <h2>Sosial Media Developer</h2>
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

        <h3>Benefit Join di Media Online</h3>
        <ul>
            <li>Mendapat motivasi dari sesama para Developer</li>
            <li>Sharing knowledge</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>

        <h3>Cara Bergabung ke Media Online</h3>
        <ol>
            <li>Mengunjungi website ini</li>
            <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
            <li>Selesai</li>
        </ol>
    </body>
</html>