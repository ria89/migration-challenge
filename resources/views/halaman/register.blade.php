<!DOCTYPE html>
<html>
    <head>
        <title>Form</title>
    </head>

    <body>
        <h2>Buat Account Baru</h2>
        <h4>Sign Up Form</h4>
        <form action="/kirim" method="post">
            @csrf
            <label>First Name:</label><br>
            <input type="text" name="firstname"><br><br>
            <label>Bio</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br><br>
            <input type="submit" value="Kirim">
        </form>
    </body>
</html>